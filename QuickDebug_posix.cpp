#include "QuickDebug.hpp"

#include <signal.h>
#include <sys/ptrace.h>
#include <sys/wait.h>



// C stuff sometimes needs to be solved with C stuff
#define QDBG_WAITFORSTATUS(expected, pid) \
    int status; \
    while (true) { \
        Helpers::throwIfSigned(waitpid(pid, &status, 0), "Failed to wait for process to reach " #expected " status"); \
        if (expected(status)) { \
            break; \
        } else { \
            Helpers::throwError("Status wait error", "Received wrong status"); \
        } \
    }


namespace QuickDebug {
void Target::attach(pid_t tPid, bool autoInterrupt) {
    Helpers::throwIfSigned(ptrace(PTRACE_SEIZE, tPid, 0, 0), "Failed to attach to process");
    // Set new pid
    pid = tPid;
    // Continue execution if told to
    if (autoInterrupt) {
        interrupt();
    }
}
void Target::detach() {
    // Don't bother if process has exited
    if (kill(pid, 0) >= 0) {
        // Deactivate all breakpoints
        for (auto& [_, breakpointList] : breakpoints) {
            for (auto& breakpoint : breakpointList.vector) {
                deactivateBreakpoint(breakpoint);
            }
        }
        // Try to detach
        if (ptrace(PTRACE_DETACH, pid, 0, 0) < 0) {
            // On error, just try to continue the process  TODO: This should never be reached, but why is it?
            try {
                continue_();
            } catch (Exception&) {}
        }
    }
    // Unset pid
    pid = 0;
}

void Target::interrupt() {
    if (running) {
        Helpers::throwIfSigned(ptrace(PTRACE_INTERRUPT, pid, 0, 0), "Failed to interrupt process");
        // Wait until process has actually been interrupted
        QDBG_WAITFORSTATUS(WIFSTOPPED, pid);
        running = false;
    }
}
void Target::continue_(int signal) {
    if (!running) {
        Helpers::throwIfSigned(ptrace(PTRACE_CONT, pid, 0, signal), "Failed to continue process execution");
        running = true;
    } else {
        // Try to do PTRACE_CONT anyways
        if (ptrace(PTRACE_CONT, pid, 0, signal) < 0 && signal) {
            // Pass signal manually since PTRACE_CONT has failed
            kill(pid, signal);
        }
    }
    // Invalidate register cache
    clearRegisterCache();
}
void Target::singleStep() {
    clearRegisterCache();
    ptrace(PTRACE_SINGLESTEP, pid, 0, 0);
    QDBG_WAITFORSTATUS(WIFSTOPPED, pid);
}

const Registers& Target::getRegisters() {
    // Try cache first
    if (registerCache.has_value()) {
        return registerCache.value();
    }
    // Fetch and store in cache now
    Registers regs;
    Helpers::throwIfSigned(ptrace(PTRACE_GETREGS, pid, nullptr, &regs), "Failed to get registers");
    registerCache = regs;
    return registerCache.value();
}
void Target::setRegisters(const Registers& regs) {
    // Set registers in process
    Helpers::throwIfSigned(ptrace(PTRACE_SETREGS, pid, nullptr, &regs), "Failed to set registers");
    // Update cache
    registerCache = regs;
}

FPRegisters Target::getFPRegisters() {
    FPRegisters regs;
    Helpers::throwIfSigned(ptrace(PTRACE_GETFPREGS, pid, nullptr, &regs), "Failed to get floating point registers");
    return regs;
}
void Target::setFPRegisters(const FPRegisters& regs) {
    Helpers::throwIfSigned(ptrace(PTRACE_SETFPREGS, pid, nullptr, &regs), "Failed to set floating point registers");
}

unsigned long Target::readWord(uint64_t addr) {
    errno = 0;
    auto word = ptrace(PTRACE_PEEKDATA, pid, addr, NULL);
    if (errno != 0) {
        Helpers::throwError("Failed to read word from process memory");
    }
    return word;
}
void Target::writeWord(uint64_t addr, unsigned long data) {
    Helpers::throwIfSigned(ptrace(PTRACE_POKEDATA, pid, addr, data), "Failed to write word to process memory");
}

uint8_t Target::readByte(uint64_t addr) {
    auto word = readWord(addr);
    return *reinterpret_cast<uint8_t*>(&word);
}
void Target::writeByte(uint64_t addr, uint8_t data) {
    auto word = readWord(addr);
    auto byte = reinterpret_cast<uint8_t*>(&word);
    *byte = data;
    writeWord(addr, word);
}

void Target::readBuffer(Addr addr, void *buffer, size_t size) {
    for (size_t it = 0; it != size; it++) {
        reinterpret_cast<uint8_t*>(buffer)[it] = readByte(addr+it);
    }
}
void Target::writeBuffer(Addr addr, const void *buffer, size_t size) {
    for (size_t it = 0; it != size; it++) {
        writeByte(addr+it, reinterpret_cast<const uint8_t*>(buffer)[it]);
    }
}

void Target::runOnce() {
    int status;
    // Check if we've missed a signal :-/
    if (!isRunning()) {
        int signal = 0;
        // Yep we have, hopefully it's a breakpoint (which we could handle now)
        if (isAtBreakpoint()) {
            // Handle that one!
            if (!handleBreakpoint()) {
                signal = SIGTRAP;
            }
        }
        // Continue execution
        continue_(signal);
    }
    // Wait for status
    Helpers::throwIfSigned(waitpid(pid, &status, 0), "Failed to wait for process to change status");
    // Process status
    processStatus(status);
}
void Target::processStatus(int status) {
    int signal = 0;
    bool tryContinue = true;
    // Process status
    if (WIFSTOPPED(status)) { // Process has been stopped
        signal = WSTOPSIG(status);
        if (signal == SIGTRAP) { // We've tripped a breakpoint
            // Handle the breakpoint
            if (handleBreakpoint()) {
                // Don't relay this signal
                signal = 0;
            }
        } else { // Just a normal signal
            auto relayMode = SignalRelayMode::neutral;
            // Execute all callbacks
            for (const auto& cb : onSignal) {
                auto tRelayMode = cb(signal);
                // Maintain relay mode
                if (relayMode == SignalRelayMode::neutral) {
                    relayMode = tRelayMode;
                }
            }
            // Don't relay signal if to be ignored
            if (relayMode == SignalRelayMode::ignore) {
                signal = 0;
            }
        }
    }
    else if (WIFSIGNALED(status)) { // Process has been terminated
        signal = WTERMSIG(status);
        // Execute all callbacks
        for (const auto& cb : onTermination) {
            cb(signal);
        }
        // Don't try to continue running
        tryContinue = false;
    }
    else if (WIFEXITED(status)) { // Process has exited
        auto exitStatus = WEXITSTATUS(status);
        // Execute all callbacks
        for (const auto& cb : onExit) {
            cb(exitStatus);
        }
        // Don't try to continue running
        tryContinue = false;
    }
    else { // We don't know what has happened
        // Execute all callbacks
        for (const auto& cb : onUnknownStatus) {
            cb(status);
        }
    }
    // Try to continue execution if possible, otherwise detach
    if (tryContinue) {
        if (isAttached()) {
            continue_(signal);
        }
    } else {
        detach();
    }
}

bool Target::isRunning() { // TODO: There must be a more efficient way to check
    // Check by trying to get registers
    user_regs_struct regs;
    return ptrace(PTRACE_GETREGS, pid, nullptr, &regs) < 0;
}
}
