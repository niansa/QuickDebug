namespace QuickDebug {
struct Exception;
enum class SignalRelayMode;
class Breakpoint;
class BreakpointList;
class Target;
}
#ifndef QUICKDEBUG_HPP
#define QUICKDEBUG_HPP
#include <string>
#include <string_view>
#include <vector>
#include <unordered_map>
#include <any>
#include <memory>
#include <functional>
#include <stdexcept>
#include <optional>
#include <cerrno>
#include <cstring>
#include <sys/types.h>
#ifdef __WIN32
#   include <windows.h>
#   include <debugapi.h>
#   include <errhandlingapi.h>
#   include <winbase.h>
#else
#   include <sys/user.h>
#endif


namespace QuickDebug {
#ifdef __WIN32
using pid_t = DWORD;
#else
using pid_t = ::pid_t;
#endif

using Word = uint64_t;
using Addr = Word;
#ifndef __WIN32
using FPRegisters = user_fpregs_struct;
#endif

struct Registers {
#   ifdef __WIN32
    Registers(const CONTEXT& c) {
        original = c;
        r15 = c.R15;
        r14 = c.R14;
        r13 = c.R13;
        r12 = c.R12;
        r11 = c.R11;
        r10 = c.R10;
        r9 = c.R9;
        r8 = c.R8;
        rax = c.Rax;
        rcx = c.Rcx;
        rdx = c.Rdx;
        rsi = c.Rsi;
        rdi = c.Rdi;
        rip = c.Rip;
        eflags = c.EFlags;
        rsp = c.Rsp;
        ss = c.SegSs;
    }
    operator CONTEXT() const {
        CONTEXT fres;
        fres = original;
        fres.R15 = r15;
        fres.R14 = r14;
        fres.R13 = r13;
        fres.R12 = r12;
        fres.R11 = r11;
        fres.R10 = r10;
        fres.R9 = r9;
        fres.R8 = r8;
        fres.Rax = rax;
        fres.Rcx = rcx;
        fres.Rdx = rdx;
        fres.Rsi = rsi;
        fres.Rdi = rdi;
        fres.Rip = rip;
        fres.EFlags = eflags;
        fres.Rsp = rsp;
        fres.SegSs = ss;
        return fres;
    }
    CONTEXT original;
#   endif

    Word r15;
    Word r14;
    Word r13;
    Word r12;
    Word rbp;
    Word rbx;
    Word r11;
    Word r10;
    Word r9;
    Word r8;
    Word rax;
    Word rcx;
    Word rdx;
    Word rsi;
    Word rdi;
#   ifndef __WIN32
    Word orig_rax;
#   endif
    Word rip;
#   ifndef __WIN32
    Word cs;
#   endif
    Word eflags;
    Word rsp;
    Word ss;
#   ifndef __WIN32
    Word fs_base;
    Word gs_base;
    Word ds;
    Word es;
    Word fs;
    Word gs;
#   endif
};


namespace SimpleInstructions {
constexpr uint8_t breakpoint = 0xCC,
                  ret = 0xC3,
                  nop = 0x90;
}


struct Exception : public std::runtime_error {
    using std::runtime_error::runtime_error;
};


enum class SignalRelayMode {
    relay,
    neutral,
    ignore
};


class Breakpoint {
    friend Target;

    std::string name;
    std::any data;
    BreakpointList *parent;
    bool active = false;

    void setActive(bool nval);

public:
    BreakpointList& getBreakpointList() {
        return *parent;
    }
    const std::string& getName() const {
        return name;
    }
    std::any &getData() {
        return data;
    }
    const std::any &getData() const {
        return data;
    }
    bool isActive() const {
        return active;
    }

    Addr getAddr() const;
};


class BreakpointList {
    friend Target;
    friend Breakpoint;

    Addr addr;
    std::vector<std::shared_ptr<Breakpoint>> vector;
    uint8_t originalByte;
    unsigned activeUserCounter = 0;

public:
    const std::vector<std::shared_ptr<Breakpoint>>& getVector() const {
        return vector;
    }
    bool isActive() const {
        return activeUserCounter;
    }
    Addr getAddr() const {
        return addr;
    }
    bool isEmpty() const {
        return vector.empty();
    }
    size_t getSize() const {
        return vector.size();
    }
    Breakpoint& operator [](size_t idx) {
        return *vector[idx];
    }
    const Breakpoint& operator [](size_t idx) const {
        return *vector[idx];
    }
} extern const emptyBreakpointList;


class Target {
    pid_t pid = 0;
#   ifdef __WIN32
    pid_t lastThread = 0;
    HANDLE procHandle;
#   endif
    bool running = false;
    std::unordered_map<Addr, BreakpointList> breakpoints;
    std::optional<Registers> registerCache;

#   ifndef __WIN32
    void processStatus(int status);
#   else
    void processEvent(const DEBUG_EVENT& event);
#   endif
    bool handleBreakpoint();
    bool isAtBreakpoint();
    void setBreakpointInstruction(BreakpointList& brk);
    void unsetBreakpointInstruction(BreakpointList& brkl);

public:
    std::vector<std::function<SignalRelayMode(int)>> onSignal;
    std::vector<std::function<void(int)>> onTermination,
                                          onExit,
                                          onUnknownStatus;
    std::vector<std::function<void(BreakpointList&)>> onBreakpoint,
                                                      onPostBreakpoint;
    std::vector<std::function<bool()>> onUnknownBreakpoint;

    Target() {}
    Target(pid_t tPid) {
        attach(tPid);
    }
    ~Target() {
        if (isAttached()) {
            detach();
        }
    }

    bool isAttached() const {
        return pid;
    }
    pid_t getPid() const {
        return pid;
    }
    const std::unordered_map<Addr, BreakpointList>& getBreakpoints() {
        return breakpoints;
    }
    BreakpointList& getBreakpointsAt(Addr addr) {
        auto res = breakpoints.find(addr);
        if (res != breakpoints.end()) {
            return res->second;
        } else {
            return const_cast<BreakpointList&>(emptyBreakpointList);
        }
    }
    std::vector<std::shared_ptr<Breakpoint>> getBreakpointsByName(std::string_view name) {
        std::vector<std::shared_ptr<Breakpoint>> fres;
        // Collect all breakpoints with that name
        for (auto& [_, breakpointList] : breakpoints) {
            for (auto& breakpoint : breakpointList.vector) {
                if (breakpoint->name == name) {
                    fres.push_back(breakpoint);
                }
            }
        }
        // Return them
        return fres;
    }
    std::shared_ptr<Breakpoint> addBreakpoint(Addr addr, std::string_view name) {
        // Create breakpoint instance
        auto brk = std::make_shared<Breakpoint>();
        brk->name = name;
        // Check if there are already breakpoints at that adress
        auto res = breakpoints.find(addr);
        if (res == breakpoints.end()) {
            // Make new list
            BreakpointList brkl;
            brkl.addr = addr;
            brkl.vector.push_back(std::move(brk));
            auto& fres = breakpoints.emplace(addr, brkl).first->second;
            fres.vector[0]->parent = &fres;
            return fres.vector[0];
        } else {
            // Append to existing list
            brk->parent = &res->second;
            return res->second.vector.emplace_back(std::move(brk));
        }
    }
    void removeBreakpoint(const std::shared_ptr<Breakpoint>& brk) {
        auto brkl = brk->parent;
        // Deactivate breakpoint
        deactivateBreakpoint(brk);
        // Remove breakpoint from breakpoint list
        for (auto it = brkl->vector.begin(); it != brkl->vector.end(); it++) {
            if (*it == brk) {
                brkl->vector.erase(it);
                break;
            }
        }
        // Remove breakpoint list if empty
        if (brkl->vector.empty()) {
            breakpoints.erase(breakpoints.find(brkl->addr));
        }
    }
    bool cachedIsRunning() const {
        return !registerCache.has_value();
    }
    void clearRegisterCache() {
        registerCache.reset();
    }
    template<typename T>
    T readObject(Addr addr) {
        uint8_t raw[sizeof(T)];
        readBuffer(addr, raw, sizeof(raw));
        return *reinterpret_cast<T*>(raw);
    }
    template<typename T>
    void writeObject(Addr addr, const T& obj) {
        auto raw = reinterpret_cast<const uint8_t*>(&obj);
        writeBuffer(addr, raw, sizeof(obj));
    }
    template<typename T>
    T top() {
        auto regs = getRegisters();
        return readObject<T>(regs.rsp);
    }
    template<typename T>
    T pop() {
        auto regs = getRegisters();
        auto fres = readObject<T>(regs.rsp);
        regs.rsp += sizeof(T);
        setRegisters(regs);
        return fres;
    }
    template<typename T>
    void push(T value) {
        auto regs = getRegisters();
        regs.rsp -= sizeof(T);
        writeObject<T>(regs.rsp, value);
        setRegisters(regs);
    }

    void attach(pid_t tPid
#               ifndef __WIN32
                ,bool autoInterrupt = false
#               endif
            );
    void detach();

    void interrupt();
    void continue_(
#           ifndef __WIN32
            int signal = 0
#           else
            bool unhandled = false
#           endif
            );
    void singleStep();

    const Registers& getRegisters();
    void setRegisters(const Registers& regs);
#   ifndef __WIN32
    FPRegisters getFPRegisters();
    void setFPRegisters(const FPRegisters& regs);
#   endif

    uint8_t readByte(Addr addr);
    void writeByte(Addr addr, uint8_t data);
    unsigned long readWord(Addr addr);
    void writeWord(Addr addr, unsigned long data);
    void readBuffer(Addr addr, void *buffer, size_t size);
    void writeBuffer(Addr addr, const void *buffer, size_t size);

    void activateBreakpoint(const std::shared_ptr<Breakpoint>& brk);
    void deactivateBreakpoint(const std::shared_ptr<Breakpoint>& brk);

    bool isRunning();
    void runOnce();
    void run();
};


class Helpers {
    friend Target;

private:
    static inline std::string lastErrorString()
#   ifndef __WIN32
    {
        return strerror(errno);
    }
#   else
    {
        auto errorMessageID = GetLastError();
        if (errorMessageID == 0) {
            return "No error";
        }
        LPSTR messageBuffer = nullptr;
        auto size = FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                                     NULL, errorMessageID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&messageBuffer, 0, NULL);
        std::string message(messageBuffer, size);
        LocalFree(messageBuffer);
        return message;
    }
#   endif

    static inline void throwError(std::string_view message, const char *cause = nullptr) {
        throw Exception(std::string(message)+": "+(cause ? cause : lastErrorString()));
    }

    template<typename T>
    static T throwIfSigned(T i, std::string_view message, const char *cause = nullptr) {
        if (i < 0) {
            throwError(message, cause);
        }
        return i;
    }
    template<typename T>
    static T throwIfZero(T i, std::string_view message, const char *cause = nullptr) {
        if (i == 0) {
            throwError(message, cause);
        }
        return i;
    }
};
}
#endif // QUICKDEBUG_HPP
