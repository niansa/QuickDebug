#include <iostream>

#include "QuickDebug.hpp"


int main(int argc, char **argv) {
    // Check usage
    if (argc < 2) {
        std::cout << "Usage: " << argv[0] << " <pid>" << std::endl;
        return EXIT_FAILURE;
    }
    // Get pid
    pid_t pid = std::atol(argv[1]);
    // Start debugger
    QuickDebug::Target target(pid);
    // Add callbacks
    target.onTermination.push_back([] (int code) {
        std::cout << "Process has exited with status code " << code << std::endl;
    });
    // Run
    target.run();
}
