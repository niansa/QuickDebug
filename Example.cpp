#include "QuickDebug.hpp"

#include <iostream>
#include <iomanip>
#include <thread>
#include <chrono>
#include <sys/types.h>
#include <sys/signal.h>



using namespace QuickDebug;

int main(int argc, char **argv) {
    // Check usage
    if (argc < 2) {
        std::cout << "Usage: " << argv[0] << " <pid>" << std::endl;
        return EXIT_FAILURE;
    }
    // Get pid
    pid_t pid = std::atol(argv[1]);
    // Start debugger
    Target target(pid);
    // Wait for a moment
    std::this_thread::sleep_for(std::chrono::seconds(1));
    // Add callbacks
    target.onSignal.push_back([&] (int signal) {
        if (signal == SIGHUP) {
            auto rip = target.getRegisters().rip;
            target.singleStep(); // Don't want to trigger breakpoint this time
            auto brk = target.addBreakpoint(rip, "Breakpoint added using SIGHUP at "+std::to_string(rip));
            target.activateBreakpoint(brk);
            std::cout << "Breakpoint added and enabled at RIP " << std::hex << rip << std::endl;
            return SignalRelayMode::ignore;
        }
        return SignalRelayMode::neutral;
    });
    target.onBreakpoint.push_back([&] (BreakpointList& brkl) {
        std::cout << "Breakpoint reached: " << brkl[0].getName() << std::endl;
    });
    // Just run
    target.run();
}
