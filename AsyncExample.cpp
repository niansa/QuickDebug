#include "QuickDebugWCoro.hpp"

#include <iostream>
#include <iomanip>
#include <thread>
#include <chrono>
#include <sys/types.h>
#include <sys/signal.h>



using namespace QuickDebug;

int main(int argc, char **argv) {
    // Check usage
    if (argc < 2) {
        std::cout << "Usage: " << argv[0] << " <pid>" << std::endl;
        return EXIT_FAILURE;
    }
    // Get pid
    pid_t pid = std::atol(argv[1]);
    // Start debugger
    AsyncTarget target(pid);
    // Wait for a moment
    std::this_thread::sleep_for(std::chrono::seconds(1));
    // Add coroutines
    async::detach([&] () -> async::result<void> {
                      while (target.isAttached()) {
                          co_await target.waitForInterrupt();
                          std::cout << "Interrupted!" << std::endl;
                      }
                  }());
    async::detach([&] () -> async::result<void> {
                      while (target.isAttached()) {
                          auto signal = co_await target.waitForSignal();
                          std::cout << "Got signal: " << signal << std::endl;
                      }
                  }());
    async::detach([&] () -> async::result<void> {
                      while (target.isAttached()) {
                          co_await target.waitForSignal(SIGHUP);
                          target.handleSignal(SignalRelayMode::ignore);
                          auto rip = target.getRegisters().rip;
                          target.singleStep(); // Don't want to trigger breakpoint this time
                          auto brk = target.addBreakpoint(rip, "Breakpoint via SIGHUP at "+std::to_string(rip));
                          target.activateBreakpoint(brk);
                          std::cout << "Breakpoint added and enabled at RIP " << std::hex << rip << std::endl;
                      }
                  }());
    async::detach([&] () -> async::result<void> {
                      while (target.isAttached()) {
                          auto brkl = co_await target.waitForBreakpoint();
                          std::cout << "Reached breakpoint: " << brkl->getVector()[0]->getName() << std::endl;
                      }
                  }());
    // Run
    target.run();
}
