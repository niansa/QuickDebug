#include "QuickDebug.hpp"

#include <windows.h>
#include <debugapi.h>
#include <winbase.h>



namespace QuickDebug {
void Target::attach(pid_t tPid) {
    // Get process handle
    procHandle = OpenProcess(PROCESS_QUERY_INFORMATION
                           | PROCESS_VM_READ
                           | PROCESS_VM_WRITE
                           | PROCESS_VM_OPERATION, FALSE, tPid);
    Helpers::throwIfZero(procHandle, "Failed to get process handle");
    // Attach
    Helpers::throwIfZero(DebugActiveProcess(tPid), "Failed to attach to process");
    Helpers::throwIfZero(DebugSetProcessKillOnExit(FALSE), "Failed to disable kill-on-exit");
    // Set new pid
    pid = tPid;
}
void Target::detach() {
    // Deactivate all breakpoints
    for (auto& [_, breakpointList] : breakpoints) {
        for (auto& breakpoint : breakpointList.vector) {
            deactivateBreakpoint(breakpoint);
        }
    }
    // Try to detach
    DebugActiveProcessStop(pid);
    // Release process handle
    CloseHandle(procHandle);
    // Unset pid
    pid = 0;
}

void Target::interrupt() {
    if (running) {
        Helpers::throwIfZero(DebugBreakProcess(procHandle), "Failed to interrupt process");
        running = false;
    }
}
void Target::continue_(bool unhandled) {
    Helpers::throwIfSigned(ContinueDebugEvent(pid, lastThread, unhandled ? DBG_EXCEPTION_NOT_HANDLED : DBG_CONTINUE), "Failed to continue process execution");
    running = true;
    // Invalidate register cache
    clearRegisterCache();
}

void Target::singleStep() {
    auto regs = getRegisters();
    regs.eflags |= 0x100;
    setRegisters(regs);
    continue_(false);
    DEBUG_EVENT debug_event;
    Helpers::throwIfZero(WaitForDebugEvent(&debug_event, INFINITE), "Failed to wait for single step to complete");
    if (debug_event.dwDebugEventCode != EXCEPTION_DEBUG_EVENT || debug_event.u.Exception.ExceptionRecord.ExceptionCode != EXCEPTION_SINGLE_STEP) {
        throw Exception("Unsupported event during singlestep");
    }
}

const Registers& Target::getRegisters() {
    // Try cache first
    if (registerCache.has_value()) {
        return registerCache.value();
    }
    // Fetch and store in cache now
    auto handle = OpenThread(THREAD_ALL_ACCESS, false, lastThread);
    CONTEXT regs;
    regs.ContextFlags = CONTEXT_ALL;
    Helpers::throwIfZero(GetThreadContext(handle, &regs), "Failed to get registers");
    CloseHandle(handle);
    registerCache = regs;
    return registerCache.value();
}
void Target::setRegisters(const Registers& regs) {
    // Set registers in process
    CONTEXT winregs = regs;
    winregs.ContextFlags = CONTEXT_ALL;
    auto handle = OpenThread(THREAD_ALL_ACCESS, false, lastThread);
    Helpers::throwIfZero(SetThreadContext(handle, &winregs), "Failed to set registers");
    CloseHandle(handle);
    // Update cache
    registerCache = regs;
}

uint8_t Target::readByte(uint64_t addr) {
    uint8_t data;
    readBuffer(addr, &data, sizeof(data));
    return data;
}
void Target::writeByte(uint64_t addr, uint8_t data) {
    writeBuffer(addr, &data, sizeof(data));
}

unsigned long Target::readWord(uint64_t addr) {
    unsigned long data;
    readBuffer(addr, &data, sizeof(data));
    return data;
}
void Target::writeWord(uint64_t addr, unsigned long data) {
    writeBuffer(addr, &data, sizeof(data));
}

void Target::readBuffer(Addr addr, void *buffer, size_t size) {
    Helpers::throwIfZero(ReadProcessMemory(procHandle, reinterpret_cast<void*>(addr), buffer, size, NULL), "Failed to write buffer to process memory");
}
void Target::writeBuffer(Addr addr, const void *buffer, size_t size) {
    Helpers::throwIfZero(WriteProcessMemory(procHandle, reinterpret_cast<void*>(addr), buffer, size, NULL), "Failed to write buffer to process memory");
}

void Target::runOnce() {
    DEBUG_EVENT debug_event;
    // Wait for debug event
    Helpers::throwIfZero(WaitForDebugEvent(&debug_event, INFINITE), "Failed to wait for debug event");
    running = false;
    // Process the event
    processEvent(debug_event);
}
void Target::processEvent(const DEBUG_EVENT& event) {
    lastThread = event.dwThreadId;
    bool handled = false;
    bool tryContinue = true;
    // Process event
    if (event.dwDebugEventCode == EXCEPTION_DEBUG_EVENT) {
        if (event.u.Exception.ExceptionRecord.ExceptionCode == EXCEPTION_BREAKPOINT) {
            // Handle the breakpoint
            handled = handleBreakpoint();
        } else {
            auto relayMode = SignalRelayMode::neutral;
            // Execute all callbacks
            for (const auto& cb : onSignal) {
                auto tRelayMode = cb(event.u.Exception.ExceptionRecord.ExceptionCode);
                // Maintain relay mode
                if (relayMode == SignalRelayMode::neutral) {
                    relayMode = tRelayMode;
                }
            }
            // Don't relay signal if to be ignored
            if (relayMode == SignalRelayMode::relay) {
                handled = true;
            }
        }
    }
    else if (event.dwDebugEventCode == EXIT_PROCESS_DEBUG_EVENT) {
        // Execute all callbacks
        for (const auto& cb : onTermination) {
            cb(event.u.ExitProcess.dwExitCode);
        }
        // Don't try to continue running
        tryContinue = false;
        handled = true;
    } else {
        for (const auto& cb : onUnknownStatus) {
            cb(event.dwDebugEventCode); //TODO: Not very useful
        }
    }
    // Try to continue execution if possible, otherwise detach
    if (tryContinue) {
        if (isAttached()) {
            continue_(!handled);
        }
    } else {
        detach();
    }
}
}
