#ifdef QUICKDEBUG_USE_COROUTINES
#ifndef _QUICKDEBUGWCORO_HPP
#define _QUICKDEBUGWCORO_HPP
#include "QuickDebug.hpp"

#include <frg/std_compat.hpp>
#include <async/recurring-event.hpp>
#include <async/queue.hpp>
#include <async/result.hpp>
#include <async/execution.hpp>



namespace QuickDebug {
class LoopService {
public:
    explicit LoopService(Target& target) : target(&target) {}

private:
    Target *target;

public:
    void wait() {
        target->run();
    }
};


class AsyncTarget : public Target {
    async::recurring_event interruptEvent,
                           breakpointEvent,
                           unknownBreakpointEvent,
                           signalEvent,
                           exitEvent,
                           terminationEvent;
    BreakpointList *currentBreakpointList;
    bool unknownBreakpointHandled;
    int currentIntValue;
    SignalRelayMode currentSignalRelayMode;

    void addCallbacks() {
        onSignal.push_back([this] (int signal) {
            interruptEvent.raise();
            currentIntValue = signal;
            currentSignalRelayMode = SignalRelayMode::neutral;
            signalEvent.raise();
            return currentSignalRelayMode;
        });
        onBreakpoint.push_back([this] (BreakpointList& brkl) {
            interruptEvent.raise();
            currentBreakpointList = &brkl;
            breakpointEvent.raise();
        });
        onUnknownBreakpoint.push_back([this] () {
            interruptEvent.raise();
            unknownBreakpointHandled = false;
            unknownBreakpointEvent.raise();
            return unknownBreakpointHandled;
        });
        onExit.push_back([this] (int status) {
            interruptEvent.raise();
            currentIntValue = status;
            exitEvent.raise();
        });
        onTermination.push_back([this] (int signal) {
            interruptEvent.raise();
            currentIntValue = signal;
            terminationEvent.raise();
        });
    }

public:
    AsyncTarget() : Target() {
        addCallbacks();
    }
    AsyncTarget(pid_t tPid) : Target(tPid) {
        addCallbacks();
    }

    auto waitForInterrupt() {
        return interruptEvent.async_wait();
    }
    async::result<int> waitForExit() {
        co_await exitEvent.async_wait();
        co_return currentIntValue;
    }
    async::result<int> waitForTermination() {
        co_await terminationEvent.async_wait();
        co_return currentIntValue;
    }
    async::result<BreakpointList*> waitForBreakpoint() {
        co_await breakpointEvent.async_wait();
        co_return currentBreakpointList;
    }
    async::result<BreakpointList*> waitForBreakpoint(const std::shared_ptr<Breakpoint>& brk) {
        while (true) {
            co_await breakpointEvent.async_wait();
            if (&brk->getBreakpointList() == currentBreakpointList) {
                co_return currentBreakpointList;
            }
        }
    }
    async::result<int> waitForSignal() {
        co_await signalEvent.async_wait();
        co_return currentIntValue;
    }
    async::result<void> waitForSignal(int signal) {
        while (true) {
            co_await signalEvent.async_wait();
            if (currentIntValue == signal) {
                co_return;
            }
        }
    }

    void handleSignal(SignalRelayMode mode) {
        if (currentSignalRelayMode == SignalRelayMode::neutral) {
            currentSignalRelayMode = mode;
        }
    }

    void markUnknownBreakpointAsHandled() {
        unknownBreakpointHandled = true;
    }

    void run() {
        async::run_forever(LoopService(*this));
    }
};
}
#endif
#endif
