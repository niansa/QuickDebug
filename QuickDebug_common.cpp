#include "QuickDebug.hpp"


namespace QuickDebug {
const BreakpointList emptyBreakpointList{};


void Breakpoint::setActive(bool nval) {
    if (nval) {
        active = true;
        parent->activeUserCounter++;
    } else {
        active = false;
        parent->activeUserCounter--;
    }
}

Addr Breakpoint::getAddr() const {
    return parent->getAddr();
}


void Target::activateBreakpoint(const std::shared_ptr<Breakpoint>& brk) {
    if (!brk->active) {
        brk->setActive(true);
        setBreakpointInstruction(*brk->parent);
    }
}
void Target::deactivateBreakpoint(const std::shared_ptr<Breakpoint>& brk) {
    if (brk->active) {
        brk->setActive(false);
        unsetBreakpointInstruction(*brk->parent);
    }
}

void Target::setBreakpointInstruction(BreakpointList &brkl) {
    brkl.originalByte = readByte(brkl.addr);
    writeByte(brkl.addr, SimpleInstructions::breakpoint);
}
void Target::unsetBreakpointInstruction(BreakpointList &brkl) {
    writeByte(brkl.addr, brkl.originalByte);
}

bool Target::isAtBreakpoint() {
    // Try to get registers
    Registers regs = getRegisters();
    // Read one byte before RIP
    auto byte = readByte(regs.rip - sizeof(SimpleInstructions::breakpoint));
    // Check if that byte is a breakpoint
    return byte == SimpleInstructions::breakpoint;
}
bool Target::handleBreakpoint() {
    // Jump back that one breakpoint instruction
    auto regs = getRegisters();
    regs.rip -= sizeof(SimpleInstructions::breakpoint);
    setRegisters(regs);
    // Find the breakpoint list
    auto brkl = getBreakpointsAt(regs.rip);
    if (!brkl.isEmpty()) { // It's our breakpoint!
        // Unset breakpoint
        unsetBreakpointInstruction(brkl);
        try {
            // Execute all callbacks
            for (const auto& cb : onBreakpoint) {
                cb(brkl);
            }
            // Check that rip hasn't changed
            if (regs.rip == getRegisters().rip) {
                // Step single instruction
                singleStep();
            }
        } catch (Exception& e) {
            // Recover breakpoint
            setBreakpointInstruction(brkl);
            // Reraise exception
            throw e;
        }
        // Set breakpoint again
        setBreakpointInstruction(brkl);
        // Execute all callbacks
        for (const auto& cb : onPostBreakpoint) {
            cb(brkl);
        }
        return true;
    } else { // This is not a breakpoint that we know of!!!
        bool handled = false;
        // Execute all callbacks
        for (const auto& cb : onUnknownBreakpoint) {
            if (cb()) {
                handled = true;
            }
        }
        // Ignore instruction
        auto rip = getRegisters().rip;
        if (readByte(rip) == SimpleInstructions::breakpoint) {
            writeByte(rip, SimpleInstructions::nop);
            singleStep();
            writeByte(rip, SimpleInstructions::breakpoint);
        } else {
            singleStep();
        }
        return handled;
    }
}

void Target::run() {
    while (isAttached()) {
        runOnce();
    }
}
}
